using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class dino_Scrip : MonoBehaviour
{
    private Rigidbody2D rb;
    private bool P1 = false;
    private bool P2 = false;
    private float vidaDino = 0;
    private float dañoP1 = 5 / 5;
    private float dañoP2 = 5 / 2;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if (P1)
        {
            if (vidaDino >= 5)
                Destroy(this.gameObject);
            P1 = false;
        }

        if (P2)
        {
            if (vidaDino >= 5)
                Destroy(this.gameObject);
            P1 = false;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision2D)
    {
        if (collision2D.gameObject.tag == "Bala1")
        {
            vidaDino = vidaDino - dañoP1;
            P1 = true;
        }

        if (collision2D.gameObject.tag == "Bala2")
        {
            vidaDino = vidaDino - dañoP2;
            P2 = true;
        }

        if (collision2D.gameObject.tag == "Bala3")
        {
            Destroy(this.gameObject);
        }
    }
}