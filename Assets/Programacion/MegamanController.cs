using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MegamanController : MonoBehaviour
{
    private SpriteRenderer sr;
    private Animator animator;
    private Rigidbody2D rb2d;
    public GameObject PelotaP1;
    public GameObject PelotaP1L;
    public GameObject PelotaP2;
    public GameObject PelotaP2L;
    public GameObject PelotaP3;
    public GameObject PelotaP3L;
    private Color originalColor;
    private float tiempo = 0;
    private float switchColorTime = 0;
    private float switchColorDelay = 0;
    void Start()
    {
        sr = GetComponent<SpriteRenderer>();
        animator = GetComponent<Animator>();
        rb2d = GetComponent<Rigidbody2D>();
        originalColor = sr.color;
    }

    void Update()
    {
        
        if (Input.GetKey(KeyCode.X))
        {
            switchColorTime += Time.deltaTime;
            if (switchColorTime > switchColorDelay)
            {
                SwitchColor();
            }
            tiempo += Time.deltaTime;
        }

        if (Input.GetKeyUp(KeyCode.X))
        {
            if (tiempo+1f >= 1 && tiempo+1f <3)
            {
                if (!sr.flipX)
                {
                    setCORRERDISPARANDOAnimator();
                    var position = new Vector2(transform.position.x + 5f, transform.position.y);
                    Instantiate(PelotaP1, position, PelotaP1.transform.rotation);
                }
                else
                {
                    setCORRERDISPARANDOAnimator();
                    var position = new Vector2(transform.position.x - 5f, transform.position.y);
                    Instantiate(PelotaP1L, position, PelotaP1L.transform.rotation);
                }
            }
            if (tiempo+1f > 3 && tiempo+1f <5)
            {
                if (!sr.flipX)
                {
                    setCORRERDISPARANDOAnimator();
                    var position = new Vector2(transform.position.x + 5f, transform.position.y);
                    Instantiate(PelotaP2, position, PelotaP2.transform.rotation);
                }
                else
                {
                    setCORRERDISPARANDOAnimator();
                    var position = new Vector2(transform.position.x - 5f, transform.position.y);
                    Instantiate(PelotaP2L, position, PelotaP2L.transform.rotation);
                }
            }

            if (tiempo+1f >5)
            {
                if (!sr.flipX)
                {
                    setCORRERDISPARANDOAnimator();
                    var position = new Vector2(transform.position.x + 5f, transform.position.y);
                    Instantiate(PelotaP3, position, PelotaP3.transform.rotation);
                }
                else
                {
                    setCORRERDISPARANDOAnimator();
                    var position = new Vector2(transform.position.x - 5f, transform.position.y);
                    Instantiate(PelotaP3L, position, PelotaP3L.transform.rotation);
                }
            }
            tiempo = 0;
        }

        setQUIETOAnimator();
        rb2d.velocity = new Vector2(0, rb2d.velocity.y);
        if (Input.GetKey(KeyCode.RightArrow))
        {
            sr.flipX = false;
            setCORRERAnimation();
            rb2d.velocity = new Vector2(15, rb2d.velocity.y); //Intanciando vector
        }
        
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            sr.flipX = true;
            setCORRERAnimation();
            rb2d.velocity = new Vector2(-20, rb2d.velocity.x);
        }

        if (Input.GetKey(KeyCode.Space))
        {
            setSALTARAnimator();
            rb2d.velocity = Vector2.up * 100;
        }
    }

    private void
    OnCollisionEnter2D(Collision2D other)
    {
        
    }
    private void SwitchColor()
    {
        if(sr.color == originalColor)
            sr.color = Color.red;
        else
            sr.color = originalColor;
        switchColorTime = 0;
        
    }


    private void setCORRERAnimation()
    {
        //RUN -correr
        animator.SetInteger("Estado", 1);
    }

    private void setSALTARAnimator()
    {
        //JUMP-saltar
        animator.SetInteger("Estado", value: 2);
    }

    private void setCORRERDISPARANDOAnimator()
    {
        animator.SetInteger("Estado", value: 3);
    }

    private void setQUIETOAnimator()
    {
        //IDLE-inactivo
        animator.SetInteger("Estado", value: 0);
    }
}